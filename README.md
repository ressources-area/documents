# Ressources-Area 
## Description
RessourceArea est un projet en lien avec la formation de sapeurs-pompiers. 
Elle regroupe les ressources nécessaire et permet la gestion des différents stages.
L'accès à l'application est uniquement réservé aux agents inscrits et connectés.

Dans un second temps, je souhaiterai implémenter un forum qui permettra aux agents connectés d'échanger.

## Installation et run
Pour permettra l'installation il faut suivre les différents ReadMe de chaque service.
- MS-Agent
- MS-Ressource
- MS-Stage
- RA-Front
Pour tester l'application vous pouver créer un utilisateur, si vous voulez tester en tant qu'admin utilisez :
identifiant : admin
mot de passe : admin
Profiter de l'application

## Owner
BRICHET Benoît - brichet.b53@gmail.com

Ressources-Area a été développé dans le cadre d'un projet final
d'une formation de développeur Java via OpenClassrooms

